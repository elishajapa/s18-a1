console.log("Hello World")

let trainer = {
	name: 'Ash Ketchum',
	age: 10,
	pokemon: ["Pikachu","Charizard","Squirtle","Bulbasaur"],
	friends: {
		hoenn: ["May","Max"],
		kanto: ["Brock","Misty"]
			 },
	talk: function(){
		console.log("Pikachu! I choose you!")
	}
}
console.log(trainer);

// Access using dot
console.log("Result of dot notation:");
console.log(trainer.name);

// Access using bracket
console.log("Result of square bracket notation:");
console.log(trainer['pokemon']);

// Access talk
console.log("Result of talk method:");
trainer.talk();


let Pokemon = {
	name: "Pikachu",
	level: 12,
	health: 24,
	attack: 12,
	tackle: function(target){
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
	},
	faint: function(){
		console.log("Pokemon fainted");
	}

}
console.log(Pokemon);

let Pokemon2 = {
	name: "Geodude",
	level: 8,
	health: 16,
	attack: 8,
	tackle: function(){
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
	},
	faint: function(){
		console.log("Pokemon fainted");
	}

}
console.log(Pokemon2);

let Pokemon3 = {
	name: "Mewtwo",
	level: 100,
	health: 200,
	attack: 100,
	tackle: function(){
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
	},
	faint: function(){
		console.log("Pokemon fainted");
	}

}
console.log(Pokemon3);

function pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
	};
	this.faint = function(){
		console.log(this.name + ' fainted.');
	}
}
Geodude.tackle(Pikachu);